%Heat diffusion ∂u/∂t = Δu 

A = zeros(100)
A (47:53,47:53) = 1
for i = 1: 200,
    conv2(A(:,:,end), [1 -2 1]); dx2 = ans(:, 2:end-1);
    conv2(A(:,:,end), [1 -2 1]'); dy2 = ans(2:end-1, :);
    A(:,:,end+1) = A(:,:,end) + .1 * (dx2 + dy2);
endfor

h = imagesc(A(:,:,1));
for i = 1:size(A,3)
    set(h, 'cdata', A(:,:,i)) % update latest frame
    pause(0.05) % feel free to reduce, but keep greater than 0 to ensure redraw
endfor
