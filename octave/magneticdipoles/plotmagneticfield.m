clear
rng = (-3:0.3:3);
dipolePositions = [kron(rng, ones(size(rng))); kron(ones(size(rng)), rng); zeros(1, size(rng,2)^2)];
m = zeros(size(dipolePositions));
n = size(dipolePositions, 2);
m(1,round(n/2)) = 1;

B = fieldfromdipoles(m, dipolePositions);
scaledB = sign(B).*abs(B).^(1/3)
quiver(dipolePositions(1,:), dipolePositions(2,:), scaledB(1,:), scaledB(2,:))
