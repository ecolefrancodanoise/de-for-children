% function F = forceondipoles(dipoleMoments, dipolePositions)
% F = ∇(m'*B) = m' * ∇(B) (m constant)

function F = forceondipoles(dipoleMoments, dipolePositions)
    dB = bgradient(dipoleMoments, dipolePositions);
    n = size(dipoleMoments,2);
    F = zeros(size(dipoleMoments));
    for k = 1:n, %contribution on k'th position
        dBk = reshape(dB(:,k,:), 3,3);
        F(:,k) = (dipoleMoments(:,k)'*dBk)';
    endfor
endfunction

%!assert(forceondipoles([0.01 0 0; 0.01 0 0]', [0 0 0; 1 0 0]'), [6.0203e-11, 0, 0; -6.0179e-11 0 0]', 1e-14);

