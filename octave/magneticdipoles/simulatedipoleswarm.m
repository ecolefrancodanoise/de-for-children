function [animationM, animationP, finalVelocities, finalw] = simulatedipoleswarm(dipoleMoments, dipolePositions, velocities, w, mass, momentOfInertia, dt, maxTime)

animationP = dipolePositions;
animationM = dipoleMoments;

for t=0:dt:maxTime,
    B = fieldfromdipoles(dipoleMoments, dipolePositions);
    T = cross(dipoleMoments, B, 1); %torque on dipoles
    F = forceondipoles(dipoleMoments, dipolePositions);
    w += T / momentOfInertia * dt; %angular velocities
    for i = 1:size(dipoleMoments,2),
        dipoleMoments(:, i) = expm([0 -w(3) w(2); w(3) 0 -w(1); -w(2) w(1) 0] *dt)*dipoleMoments(:,i);
    endfor
    velocities += F / mass * dt;
    dipolePositions += velocities * dt;
    animationP(:,:,end+1) = dipolePositions;
    animationM(:,:,end+1) = dipoleMoments;
endfor

finalVelocities = velocities;
finalw = w;
