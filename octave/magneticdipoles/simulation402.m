% Four dipoles, one meter apart, orthogonal, loop of 100cm², 1A, mass 1kg
% The configuration will start to contract, no rotation, zero (i.e. evened out) torque on the dipoles
% After inverting every second dipole: expands, still no rotation zero torque

clear
dipolePositions = [0 0 0; 1 0 0; 1 1 0; 0 1 0]';
dipoleMoments = [1 -1 0; 1  1 0; -1 1 0; -1 -1 0]' * 0.01 / sqrt(2);
velocities = zeros(size(dipolePositions));
w = zeros(size(dipolePositions));
mass = 1.0;
momentOfInertia = 1.0*0.05^2;
dt = 40;

B = fieldfromdipoles(dipoleMoments, dipolePositions)
T = cross(dipoleMoments, B, 1)
F = forceondipoles(dipoleMoments, dipolePositions)

[animationM{1}, animationP{1}, finalVelocities, finalw] = simulatedipoleswarm(dipoleMoments, dipolePositions,
                                                                        velocities, w, mass, momentOfInertia, dt, 60000);

%reverse every second dipole and continue
newDipoleMoments = animationM{1}(:,:,end) * diag([1 -1 1 -1]);
newDipolePositions = animationP{1}(:,:,end);
B2 = fieldfromdipoles(newDipoleMoments, newDipolePositions)
T2 = cross(newDipoleMoments, B2, 1)
F2 = forceondipoles(newDipoleMoments, newDipolePositions)

[animationM{2}, animationP{2}] = simulatedipoleswarm(newDipoleMoments, newDipolePositions,
                                                 finalVelocities, finalw, mass, momentOfInertia, dt, 60000);

exportanimationtogif("animation402.gif", cat(3, animationM{:}), cat(3, animationP{:}), 200)

