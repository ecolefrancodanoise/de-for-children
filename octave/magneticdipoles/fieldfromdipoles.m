% function B = fieldfromdipoles(dipoleMoments, r)
%
% Magnetic field generated at r from dipoles placed at r with dipole moment m


function B = fieldfromdipoles(dipoleMoments, dipolePositions)
    if size(dipoleMoments) != size(dipolePositions), error("Dimension mismatch"); endif
    B = zeros(size(dipoleMoments));
    n = size(dipoleMoments,2);
    for i = 1:n,
        %contribution from the i'th dipole
        r = dipolePositions - dipolePositions(:,i) * ones(1, n);
        mi = dipoleMoments(:,i);
        if mi == [0 0 0]', continue; endif
        for j = 1:n,
            %contribution to the j'th position
            if i == j; continue; endif
            rj = r(:,j);
            Bj = 1.26e-6 / 4 / 3.14 * (3*rj*(mi'*rj) / norm(rj)^5 - mi / norm(rj)^3);
            B(:,j) += Bj;
        endfor
    endfor
endfunction

%!assert(fieldfromdipoles([1 0 0]', [1 0 0]'), [0 0 0]', 1e-8)

%loop of 1A, surface of 100cm² distance 10 cm:
%!assert(fieldfromdipoles([0.01 0 0]', [0.1 0 0]'), [0 0 0]', 1e-8)

%!assert(fieldfromdipoles([1 0 0; 1 0 0]', [0 0 0; 1 0 0]'), [2.0064e-07 0 0; 2.0064e-07 0 0]', 1e-10)
%!assert(fieldfromdipoles([1 0 0; 0 0 0]', [0 0 0; 1 0 0]'), [0 0 0; 2.0064e-07 0 0]', 1e-10)
%!assert(fieldfromdipoles([1 0 0; 0 0 0]', [0 0 0; 1.1 0 0]'), [0 0 0; 1.5074e-07 0 0]', 1e-10)
