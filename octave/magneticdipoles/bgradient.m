function dB = bgradient(dipoleMoments, dipolePositions)
    n = size(dipoleMoments,2);
    dB = zeros(3,n,3);
    B0 = fieldfromdipoles(dipoleMoments, dipolePositions);
    d = 0.0001;
    for j = 1:n,
        for k = 1:3,
            dkDipolePositions = dipolePositions;
            dkDipolePositions(k,j) += d;
            dB(:,j,k) = ((fieldfromdipoles(dipoleMoments, dkDipolePositions) - B0) / d)(:,j);
        endfor
    endfor
endfunction
