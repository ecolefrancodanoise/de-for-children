function exportanimationtogif(fileName, animationM, animationP, stepSize)

symbols = ['a':'z' 'A':'Z' '0':'9'];
pdfFilename = [symbols(randi(numel(symbols),[1 20])) ".pdf"];

figure()
for i = 1:stepSize:size(animationP,3)
    quiver (animationP(1,:,i), animationP(2,:,i), animationM(1,:,i), animationM(2,:,i), 0.2)
    axis ([-2 2 -2 2])
    axis equal
    print(pdfFilename, '-append')
endfor

im = imread(pdfFilename, "Index", "all");
imwrite(im, fileName, "DelayTime", .5)
