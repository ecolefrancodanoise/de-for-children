% Four dipoles, one meter apart, orthogonal, loop of 100cm², 1A, mass 1kg, I = 0.025 kg*m²
% The configuration will start spinning and contract.

clear
dipolePositions = [0 0 0; 1 0 0; 1 1 0; 0 1 0]';
dipoleMoments = [0.01 0 0; 0.0 0.01 0; -0.01 0 0; 0.0 -0.01 0]';
velocities = zeros(size(dipolePositions));
w = zeros(size(dipolePositions));
mass = 1.0;
momentOfInertia = mass*0.05^2;
dt = 40;

[animationM{1}, animationP{1}, velocities, w] = simulatedipoleswarm(dipoleMoments, dipolePositions,
                                                                    velocities, w, mass, momentOfInertia, dt, 120000);


exportanimationtogif("animation403.gif", cat(3, animationM{:}), cat(3, animationP{:}), 100)
