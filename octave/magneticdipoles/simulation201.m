%Two dipoles, one meter apart, orthogonal, loop of 100cm², 1A, mass 1kg

clear
dipolePositions = [0 0 0; 1 0 0]';
dipoleMoments = [0.01 0 0; 0.0 0.01 0]';
velocities = zeros(size(dipolePositions));
w = zeros(size(dipolePositions));
mass = 1.0;
momentOfInertia = 1.0;
dt = 50;

[animationM{1}, animationP{1}, finalVelocities, finalw] = simulatedipoleswarm(dipoleMoments, dipolePositions, velocities, w, mass, momentOfInertia, dt, 200000)

exportanimationtogif("animation.gif", animationM{1}, animationP{1}, 200)
