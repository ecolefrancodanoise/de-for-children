% Four dipoles, one meter apart, orthogonal, loop of 100cm², 1A, mass 1kg
% The configuration will start spinning and contract.
% After a while, we invert every second diple - the structure expands.

clear
dipolePositions = [0 0 0; 1 0 0; 1 1 0; 0 1 0]';
dipoleMoments = [0.01 0 0; 0.0 0.01 0; -0.01 0 0; 0.0 -0.01 0]';
velocities = zeros(size(dipolePositions));
w = zeros(size(dipolePositions));
mass = 1.0;
momentOfInertia = 1.0; %NB: large moment of inertia
dt = 40;

[animationM{1}, animationP{1}, finalVelocities, finalw] = simulatedipoleswarm(dipoleMoments, dipolePositions,
                                                                        velocities, w, mass, momentOfInertia, dt, 160000);

%reverse ever second dipole and continue
[animationM{2}, animationP{2}] = simulatedipoleswarm(animationM{1}(:,:,end) * diag([1 -1 1 -1]), animationP{1}(:,:,end),
                                                 finalVelocities, finalw, mass, momentOfInertia, dt, 160000);

exportanimationtogif("animation401.gif", cat(3, animationM{:}), cat(3, animationP{:}), 200)
