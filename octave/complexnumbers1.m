%This is a small exercise illustrating the use of complex numbers
%We consider two electrons. The first is moving along the unit circle. The second is at (10,10)
%We want to determine the distance d0 between the second electron at the time t = 100
%and the first electron at the time t-d0/c, that is its position relative to the first
%electron given that the information propagates at the speed of light


c = 1 %speed of light
t = 100 %just some point in time

%using two variables x and y to describe each electrons position
d0 = 0
xz = 10
yz = 10
for k = 1 : 10
    xe = cos(t - d0/c);
    ye = sin(t - d0/c);
    d0 = sqrt((xz-xe)^2 + (yz-ye)^2)
endfor

%describing their positions as vectors
d0 = 0
z = [10,10]
for k = 1 : 10
    d0 = norm(z-[cos(t-d0/c), sin(t-d0/c)])
endfor

%using complex numbers
d0 = 0;
z = 10 + 10 * i
for k = 1 : 10
    d0 = norm(z - exp(i*(t - d0/c)))
endfor
