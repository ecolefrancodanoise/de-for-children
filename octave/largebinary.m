% Large binary system
%
% A Newtonian model of a binary star system consisting of two stars ~50 times
% heavier than the sun, rotating symetrically around the origin
%
% Velocity is calculated by assuming a circular motion and deriving
% r * cos(ωt) twice and equating to acceleration
% - r * ω² * cos(ωt) =  - G * M *  r * cos(ωt) / (2*r)³


M = 1e32;
G = 6.6e-11;
p = [1e14, 0]';
v = [0, sqrt(M*G/(norm(p)*8))]';
dt = 1e9;
positions = [];

for i=1:200,
   positions(:, end+1) = p;
   d = 2 * norm(p);
   a = - G * M * p / d^3;
   v = v + a * dt;
   p = p + v * dt;
endfor

plot(positions(1,:), positions(2, :)); axis equal
