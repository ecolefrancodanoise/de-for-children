nbPoints = 20
[xx,yy] = meshgrid(-1000:1000, -1000:1000);
xxx = kron3(ones(1,1,nbPoints), xx);
yyy = kron3(ones(1,1,nbPoints), yy);
particleX = -500 + 50 * floor(rand(nbPoints,1)*nbPoints);
particleY = -500 + 50 * floor(rand(nbPoints,1)*nbPoints);
xxx += kron3(reshape(particleX, 1, 1, nbPoints), ones(size(xx)));
yyy += kron3(reshape(particleY, 1, 1, nbPoints), ones(size(yy)));
rrr = (xxx.^2 + yyy.^2).^0.5;
zzz = cos(2*pi/50*rrr).*(rrr+0.1).^-2;
zz = sum(zzz,3);
imagesc(log(abs(zz))); axis equal
