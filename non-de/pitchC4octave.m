%Octave starting at pitch C
% notes = {"C"  "C♯/Db" "D"  "D♯/Eb" "E" "F"  "F♯/Gb"  "G"  "G♯/Ab"  "A"  "A♯/Bb"  "B"}
% B minor (C♯, D, E, F♯, G, and A, B): notes{[2 3 5 7 8 10 12]}

frequencies = 220 * 2 .^ ((3:14)/12)
frequencies([2 3 5 7 8 10 12])
