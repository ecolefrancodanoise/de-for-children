input = [
0 0 0 2 6 0 7 0 1
6 8 0 0 7 0 0 9 0
1 9 0 0 0 4 5 0 0
8 2 0 1 0 0 0 4 0 
0 0 4 6 0 2 9 0 0
0 5 0 0 0 3 0 2 8
0 0 9 3 0 0 0 7 4
0 4 0 0 5 0 0 3 6
7 0 3 0 1 8 0 0 0
]
[a, b] = find(input);
userConstraints = zeros(9,9,9); 
for i = 1:length(a)
    userConstraints(a(i), b(i), input(a(i), b(i))) = 1;
endfor
justSomeCostVector = (1:9*9*9);
columnWiseOne = kron(eye(81), ones(1,9));
rowWiseOne = kron(kron(eye(9), ones(9)), eye(9));
depthWiseOne = kron(ones(1,9), eye(81));
sum3x3 = kron(eye(27), kron(ones(1,3), kron(eye(3),[ones(1,3)])));
constraintMatrix = [columnWiseOne; rowWiseOne; depthWiseOne; sum3x3; userConstraints(:)'];
constraintRHS = ones(size(constraintMatrix, 1), 1); constraintRHS(end) = sum(userConstraints(:));
solution = glpk(justSomeCostVector, constraintMatrix, constraintRHS, zeros(729,1), ones(729,1));
sol = sum(reshape(solution .* kron(1:9, ones(1,81))', [9,9,9]), 3)
