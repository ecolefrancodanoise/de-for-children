A = [
5 4 3 6 2 1
1 2 4 3 5 6
5 4 6 3 1 2
2 1 3 4 5 6
4 3 2 5 6 1
6 1 5 2 3 4
6 4 5 2 3 1
5 3 4 2 6 1
2 1 6 5 3 4
2 6 4 1 5 3
1 4 5 3 2 6
6 3 4 5 1 2
5 1 3 4 2 6
5 3 6 1 2 4
3 1 5 4 6 2
3 4 1 6 2 5
5 6 4 1 2 3
1 4 6 5 2 3
2 6 3 4 5 1
2 6 5 3 4 1
]

b = [6 3 4 2 4 1]
[nbPupils, nbBooks] = size(A)
costVector = A(:);
constraintsMaxBooks = kron(eye(length(b)),ones(1, nbPupils));
constraintsOneBookPerChild = kron(ones(1, nbBooks), eye(nbPupils));
solution = glpk(costVector, [constraintsMaxBooks; constraintsOneBookPerChild], [b'; ones(nbPupils, 1)]);
reshape(solution, size(A))
solution'*costVector
