w = [1105 45 1950 1235 1305 115 995 5 5 745 10 25 1800 335 145 3495 10 175 65 360 545 5 1200 25 5 985 120 10 425 4085 ]
nbAnimals = length(w);
nbPlanes = 5;
onePlanePerAnimal = kron(ones(1,5), eye(30));
distributeWeightEqually = kron(eye(5), w);
A = [onePlanePerAnimal; distributeWeightEqually]
b = [ones(nbAnimals, 1); ceil(sum(w)/nbPlanes/5)*5 * ones(nbPlanes, 1)]
c = zeros(nbAnimals*nbPlanes,1);
s = glpk(c, A, b,
         [], [],
         char([ones(nbAnimals, 1)*'S'; ones(nbPlanes, 1)*'U']),
         char(ones(size(c))*'I')
         )
reshape(s, nbAnimals, nbPlanes)
