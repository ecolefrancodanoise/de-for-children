Linear Assignment Problems
==========================

A teacher's dilemma
-------------------
https://astra-nova-3dm.web.app/

[Solution](booklap.m)


Zoo transport problem
---------------------
![Zoo transport problem](zootransportproblem.png)

[Solution](zootransport.m)

A 9x9 sudoku solver
-------------------
[Solution](sudokusolver.m)
