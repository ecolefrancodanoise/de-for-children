#include <gmpxx.h>
#include <iostream>

int main(int argc, char** argv) {
    if (argc != 3) {
        printf("Usage: echo <message> | encode <n> <e>\nGenerates encoded <message> into <c>. <c> can then be decoded with\n\nencode <n> <d> <c>\n\nwhere <d> is the private key.\n");
        return 1;
    }
    while (std::cin) {
        std::string message;
        getline(std::cin, message);
        if (message.length() > 0) {
            mpz_class n(argv[1], 62);
            mpz_class e(argv[2], 62);
            const char* message_str = message.c_str();
            mpz_class m;
            m.set_str(message, 62);
            mpz_class c;
            mpz_powm_sec(c.get_mpz_t(), m.get_mpz_t(), e.get_mpz_t(), n.get_mpz_t());
            gmp_printf("%s\n", c.get_str(62).c_str());
        }
    }
    return 0;
}
