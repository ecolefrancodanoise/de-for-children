#include <gmpxx.h>

mpz_class generateprime(int bitsize) {
    gmp_randclass rr(gmp_randinit_default);
    rr.seed(time(NULL));
    while (true) {
        mpz_class a = rr.get_z_bits(bitsize);
        if (mpz_probab_prime_p(a.get_mpz_t(), 100) > 0) {
            return a;
        }
    }
}

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("Usage: generateprimes <keysize in bits>\nGenerates public key (n, e) and private key (n, d)\n");
        return 1;
    }
    const int keysize = atoi(argv[1]);
    mpz_class p, q, d, e, n, phi;
    p = generateprime(keysize / 2);
    q = generateprime(keysize / 2 + 2);
    d = generateprime(keysize / 2 + 2);
    n = p * q;
    phi = (p - 1) * (q - 1);
    mpz_invert(e.get_mpz_t(), d.get_mpz_t(), phi.get_mpz_t());
    gmp_printf("n: %s\n", n.get_str(62).c_str());
    gmp_printf("d: %s\n", d.get_str(62).c_str());
    gmp_printf("e: %s\n", e.get_str(62).c_str());
    return 0;
}
