Getting started with RSA.
=========================
install the relevant packages:
```
sudo apt-get install libgmp-dev libgmpxx4ldbl qrencode zbar-tools
```
Compile the encode program:
```
g++ encode.cpp -lgmp -lgmpxx -o encode
```
Assuming the following 64-bit keys:
```
n: bPD0bwGqiUD
e: D5YNpl
d: Z8nfzJZhgdh
```
Encrypt the string *donaldduck*:
```
echo donaldduck | ./encode bPD0bwGqiUD D5YNpl
```
Which gives the encoded string *6AcOnEt9LlQ*

Then decoding it with:
```
echo 6AcOnEt9LlQ | ./encode bPD0bwGqiUD Z8nfzJZhgdh
```
Which gives the decoded string *donaldduck*

Signing Messages
================

Signing the message *Tillykke* with the private key *Z8nfzJZhgdh*
```
echo Tillykke | ./encode bPD0bwGqiUD Z8nfzJZhgdh
```
Which gives *PWwpqYZFCb9*
Pipe thus into qrencode
```
echo Tillykke | ./encode bPD0bwGqiUD Z8nfzJZhgdh | qrencode -o tillykke.png
```
Generates a QR code  in the file tillykke.png wich can be sacnned using zbarcam and decoded
```
zbarcam --raw | ./encode bPD0bwGqiUD D5YNpl
```
