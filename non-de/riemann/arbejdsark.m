% En visuel, hands-on introduktion til Riemanns Zeta-funktion
%
% Under dette forløb vil du få indgående kendskab til og forståelse af
% Zeta-funktionen, et matematisk værktøj der dels har en række praktiske
% anvendelser på det naturvidenskabelige område, dels illustrerer nogle
% underfundige sammenhænge i universets informationsstruktur.
%
% Undervejs vil du lære at bruge nogle alment nyttige matematiske og
% programmeringstekniske værktøjer og få et fungerende eksempel på
% avanceret algoritmik at tage med videre.
%
% Oversigt:
% 1. Introduktion til matrixregning og komplekse tal
% 2. Den s-afhængige sekvens (Dirichlet)
% 3. Basisfunktionerne
% 4. Den underfundige udfordring

% 1. Introduktion til matrixregning og komplekse tal
% Udfør i programmet Octave (eller Matlab) hver af de følgende kommandoer
% og forklar, hvad de gør:

v = 1:10
primes(1000)
primes(1000)(1:10)
A = diag(primes(1000)(1:10))
A * v'
A(1:end, 1:3)
sqrt(-25)
exp(i * pi) + 1
(1 + i) * exp(i * pi / 4)

% Hvad gør de?


% 2. Den s-afhængige sekvens (Dirichlet)
% Den komplekse vektor D bygges op som følger ud fra input s, fx

s = 5.5 + 21.022040 * i
nbTerms = 10;
D = (-1).^(0:nbTerms-1)'.*(1:nbTerms)'.^-s * 0.5 / (1.0 - 2 * 0.5^s)

plot(D)

% Relationen til primtal:
% Udregn nu

prod(1./(1-primes(10000)(1:nbTerms).^-s))
sum(D) * 2

% Dette kaldes "Eulers produktformel", hvis du vil læse mere om det.
% OBS: den gælder kun når Re(s) > 1.

% 3. Basisfunktionerne
% For Re(s) < 1 har seriens grænseværdier formen "-inf + inf" og man er derfor
% nødt til at omskrive den, lidt i stil med det man gør når man normalt finder
% grænseværdier - dette kaldes her "analytisk fortsættelse". Dette kan fx gøres ved at
% anvende nogle basisfunktioner, der "flytter værdi " fra halen med de høje n op til
% "hovedet" med de lave n.
% I praksis gøres dette ved at vægte en sum af basisfunktioner med elementerne i D og
% summere alle elementerne i den resulterende vektor.
% Basisfunktionerne bygges op som følger (her bruger vi en normaliseret Pascals
% trekant:

s = 0.5 + 21.022040 * i
nbTerms = 100;
D = (-1).^(0:nbTerms-1)'.*(1:nbTerms)'.^-s * 0.5 / (1.0 - 2 * 0.5^s)

B = zeros(nbTerms);
B(1,1) = 1;
for n = 1:nbTerms - 1,
    B(n+1, 1:n+1) = conv(B(n, 1:n), [1 1] / 2);
endfor
B

% Beskriv, hvordan matricens struktur ser ud.
% Plot en af basisfunktionerne:

plot(B(:, 5))

% Sammenlign den oprindlige Dirichlet-serie med den transformerede
plot(abs(D)); hold on; plot(real(B*D)); grid on; hold off

%Udregn nu
zsum = sum(B*D)

% 4. Den underfundige udfordring
% Nu indkapsler vi den ovenstående funktionalitet i en funktion

function [zsum, B, D] = zeta(s)
    nbTerms = 100;
    B = zeros(nbTerms);
    B(1,1) = 1;
    for n = 1:nbTerms - 1,
        B(n+1, 1:n+1) = conv(B(n, 1:n), [1 1] / 2);
    endfor
    D = (-1).^(0:nbTerms-1)'.*(1:nbTerms)'.^-s * 0.5 / (1.0 - 2 * 0.5^s);
    zsum = sum(B*D);
endfunction

% Riemann-hypotesen
% Udregn nu et såkaldt trivielt nulpunkt (lige, negative tal)
zeta(-2)

% Og følgende ikke-trivielle nulpunkter:
zeta(0.5 + i * 14.134725)
zeta(0.5 + i * 21.022040)
zeta(0.5 + i * 25.010858)

% Kan du bevise, at alle ikke-trivielle nulpunkter ligger på aksen Re(s) = 1/2?
% Undersøg fx, hvordan nulpunkterne fordeler sig på Re(s)= 1/2-aksen:

y = -40:0.01:40;
subplot(2,1,1);
plot(y, abs(arrayfun(@zeta, 0.5 + i * y)))

%og sammenlign med fx Re(s)= 0.6-aksen

subplot(2,1,2);
plot(y, abs(arrayfun(@zeta, 0.6 + i * y)))

[zsum, B, D] = zeta(0.5 + i * 14.134725)

% Du kan også tegne alle basisfunktioner vægtet med Dirichlet s-værdierne,
% som som bekendt sammenlagt giver 0 for visse værdier på Re(s) = 1/2 -aksen

[zsum, B, D] = zeta(0.5 + i * 14.134725);
plot(B .* (ones(length(D), 1) * real(D')))

[zsum2, B2, D2] = zeta(0.6 + i * 14.134725);

