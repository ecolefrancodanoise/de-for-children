/*
 * p5BarGraph(x,y,values)
 * Draws a bar graph at the position x and y with the height of values
 */
var p5BarGraph = function(x,y,values){
    fill(0,255,0);
    for(var i=0;i<values.length;i +=1){
        rect(x+20*i,y,10,-values[i])
    };
}
