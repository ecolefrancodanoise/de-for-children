/*
 * p5table(x,y,values)
 * 
 */
var drawp5table = function(x,y,tablevalues) {
    const nbrows = tablevalues.length
    const nbcolumns = tablevalues[0].length
    for(var i = 0; i < nbrows; i++){
        //drawing horizontal lines
        line(x, y+20*i, x+tablevalues[0].length*80, y+20*i);
        for(var j = 0; j<nbcolumns; j++){
            //drawing vertical lines
            line(x+80*j, y, x+80*j, y+tablevalues.length*20);
            text("" +Math.round(tablevalues[i][j]), x+80*j+5, y+20*i+15);
        }
    }
    line(x+nbcolumns*80,y,x+nbcolumns*80,y+nbrows*20)
    line(x,y+nbrows*20,x+nbcolumns*80,y+nbrows*20);
};
