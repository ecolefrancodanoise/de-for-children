From The Dust
=============
These images are snapshots of animations illustrating stochastic differential equations. They describe systems that evolve randomly in small steps (differences) over time. Many of the concepts involved are graspable by children and are a fun way to learn programming. Click on the images to see the animations.

A Random Walk In The Park
-------------------------
[![A Random Walk In The Park](random-walk.png)](https://ecolefrancodanoise.gitlab.io/de-for-children/processingjs/p5/particle-spreading.html)

This simulation is about Brownian motion. The particles all start in the middle and then randomly start spreading out. In each time step the positon of each particle changes with a small randomamount. The phenomenon is widely used in physics for instance to desribe diffusion. One particular aspect is that the particles spread out rapidly in the beginning and drastically slow down.

Set Them Free
-----------------
[![Random Velocities](random-velocities.png)](https://ecolefrancodanoise.gitlab.io/de-for-children/processingjs/p5/random-velocities.html)

In this simulation the particles have a stochastically changing velocity beginning at zero. Looks like an explosion.

Birds Of A Feather
------------------
[![Birds of a feather](random-followers.png)](https://ecolefrancodanoise.gitlab.io/de-for-children/processingjs/p5/randomfollowers.html)

An N-body model of flock dynamics. Each particle will align its velocity on particles in front of it that are sufficiently close. Also a good introduction to scalar products.

To The Stars
------------
[![Star formation](nebula-simulation.png)](https://ecolefrancodanoise.gitlab.io/de-for-children/processingjs/p5/nebula-simulation.html)

How do stars form? Here is a simulation of a particle cloud where particles attract each other according to Newton's law of gravity. If two particles are close enough they assemble into one.
