Lad os først starte med at lave en boldt og sige til programet hvor den er.
Boldens **position** er 'y' og 'x'.

Prøv at se hvad der sker når du ændrer 'y' og 'x':

```
var x=100;
var y=20;

draw = function() { 
 background(0, 0, 0);
 
 fill(30, 255, 0);
 ellipse(x, y,20,20);
};
```

Den boldt er ikke særlig spændende.
Lad os give den en **hastighed** 'yp'.
Vi skal også husk at sige til programet at bolden skal brug den **hastighed**.
Så vi koder vores første **operation** 'y=y+yp'.

Hvad sker der når du endrer på 'yp'?:

```
var x=100;
var y=20;
var yp=10;

draw = function() { 
 background(0, 0, 0);
 
 fill(30, 255, 0);
 ellipse(x, y,20,20);
 y=y+yp;
};
```

I den virkelige værden så falder ting ikke med en konstant hastighed.
Ting falder altid med en **acceleration**.
Så nu giver vi bolden en **acceleration** med 'ypp'.
Husk også at kode den anden **operation** 'yp=yp+ypp'.

Prøv at ændre på **accelerationen**, men husk at på Jorden er den 9.8:

```
var x=100;
var y=20;
var yp=0;
var ypp=10;

draw = function() { 
 background(0, 0, 0);
 
 fill(30, 255, 0);
 ellipse(x, y,20,20);
 yp=yp+ypp;
 y=y+yp;   
};
```

Wow, det gik lidt for hurtigt...
Vi koder vi **differenciallet af tid** med 'dt' for at kunne styre tidens fremgang.

Hvad sker der når man ændrer 'dt'?:

```
var x=100;
var y=20;
var yp=0;
var ypp=9.8;
var dt=0.1;

draw = function() { 
 background(0, 0, 0);
 
 fill(30, 255, 0);
 ellipse(x, y,20,20);
 yp=yp+ypp*dt;
 y=y+yp*dt;
};
```

