Jorden og Mars har forskellige **tygndekraft**.
Jorden har 9.8, Mars har 3.7.
Nu skal vi kode Mars ind i vores simulation, med **variabler** og **operationer**.

Jordens og Marses værdier må ikke hede det samme.
Derfor definere vi Jordens værdier med '0' (x0, y0, y0p, y0pp) og Mars med '1':

```
//Jorden
var x0=100;
var y0=20;
var y0p=0;
var y0pp=9.8;

//Mars
var x1=300;
var y1=20;
var y1p=0;
var y1pp=3.7;

var dt=0.05;

draw = function() { 
 background(0, 0, 0);
 
 //Jorden
 fill(30, 255, 0);
 ellipse(x0, y0,20,20);
 y0p=y0p+y0pp*dt;
 y0=y0+y0p*dt;
 
 //Mars
 fill(255, 89, 0);
 ellipse(x1, y1,20,20);
 y1p=y1p+y1pp*dt;
 y1=y1+y1p*dt;
};
```



