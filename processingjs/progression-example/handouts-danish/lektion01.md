Programmering og differentialligninger
======================================

Differentialligninger bruges til alt muligt ude i den virkelige verden. Til at designe ting, til at forstå økonomi, indenfor videnskaben og meget, meget mere. Man kunne måske endda sige, at det er det mest brugte værktøj fra matematikken.

Normalt lærer man om differentialligninger i gymnasiet, men hvis man kan programmere bare en lillebitte smule, kan man faktisk begynde at forstå dem meget tidligere. Det er det, denne opgave drejer sig om.  Når du er færdig med opgaven vil du:
* kunne programmere en smule
* have forstået hvad man bruger variable til
* have forstået hvad man bruger differentialligninger til

Gå ind på siden  https://www.khanacademy.org/computer-programming/new/pjs og skriv følgende program, som beskriver en sol og en planets bevægelse:

```
var x=300;
var y=300;

draw = function() {
    background(26, 19, 51);
    fill(255, 255, 0);
    ellipse(200, 200, 20, 20);
    fill(73, 189, 209);
    ellipse(x, y, 10, 10);
    x=x+1;
    y=y+1;
};
```

Hvad er x? og y?
Passer bevægelsen med en rigtig planetbane?
Kan du få planeten til at bevæge sig hurtigere?

Nu skal vi gøre bevægelsen mere realistisk. Ændr programmet til følgende:

```
var x=100;
var y=-100;
var xp=1;
var yp=3;
var dt = 0.2;

draw = function() {
    background(26, 19, 51);
    //solen
    fill(255, 255, 0);
    ellipse(200, 200, 20, 20);
    fill(73, 189, 209);
    ellipse(x+200, y+200, 10, 10);
    var d=Math.sqrt(x * x + y * y);
    var xpp= - 3000 * x/Math.pow(d, 3);
    var ypp= - 3000 * y/Math.pow(d, 3);
    xp=xp+xpp*dt;
    yp=yp+ypp*dt;
    x=x+xp*dt;
    y=y+yp*dt;
};
```

Prøv at ændre x, y, xp og yp? Hvad sker der?
Prøv at ændre tallet 3000. Hvad sker der?

Prøv nu at lave din egen tegning, der bevæger sig.
