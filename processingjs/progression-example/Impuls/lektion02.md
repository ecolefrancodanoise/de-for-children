Impuls, masse og hastighed II
=========================================

Intro:

Vi skal lige have styr på vores data


```
// Bold 1
var d1 = 20;
var m1 = 20;
var v1 = 10;
var x1 = 10;

// Bold 2
var d2 = 5;
var m2 = 5;
var v2 = 0;
var x2 = 60;

var p1= abs(v1*m1);
var p2= abs(v2*m2);

var dt = 0.1;

draw = function() {
    background(255, 255, 255);
    
    for (var i = 0; i < 400; i+=20) {
        stroke(133, 133, 133);
        line(i,0,i,400);
        stroke(0, 0, 0);
    }
    // Data
    fill(255, 255, 255);
    rect(0,0,400,100);
    textSize(20);
    fill(255, 115, 0);
    text("Masse 1: "+ m1,20,25);
    text("Hastighed 1: "+ round(v1),20,60);
    text("Impuls 1: "+ round(p1),20,95);
    fill(31, 91, 255);
    text("Masse 2: " +m2,220,25);
    text("Hastighed 2: " +round(v2),220,60);
    text("Impuls 2: " +round(p2),220,95);
    
            
    // Bold 1
    fill(255, 115, 0);
    ellipse(x1,200,d1,d1); 
    
    // Bold 2
    fill(31, 91, 255);
    ellipse(x2,200,d2,d2); 
    
        p1=abs(v1*m1);
        x1= x1+v1*dt;
       
        p2=abs(v2*m2);
        x2= x2+v2*dt;
    
        
        if(x2-d2/2 < x1+d1/2){
            p2=p2+p1;
            p1=0;
            v2 = p2/m2;
            v1 = (p1/m1);
        }
};
```

Wow nice data

Nu skal vi lave en rigtig simulation!

{\displaystyle {\begin{array}{ccc}v_{1}&=&{\dfrac {m_{1}-m_{2}}{m_{1}+m_{2}}}u_{1}+{\dfrac {2m_{2}}{m_{1}+m_{2}}}u_{2}\\[.5em]v_{2}&=&{\dfrac {2m_{1}}{m_{1}+m_{2}}}u_{1}+{\dfrac {m_{2}-m_{1}}{m_{1}+m_{2}}}u_{2}\end{array}}}

Vi skal faktisk ikke ændre særlig meget.

Vi skal lave en ny variabel 'ov1'

Og vi skal ændre kode for når vi tjekker for sammenstød inde i 'if(x2-d2/2 < x1+d1/2)'

```
// Bold 1
var d1 = 20;
var m1 = 20;
var v1 = 10;
var x1 = 10;

// Bold 2
var d2 = 5;
var m2 = 5;
var v2 = 0;
var x2 = 60;

var p1= v1*m1;
var p2= v2*m2;

var dt = 0.1;

var ov1;

draw = function() {
    background(255, 255, 255);
    
    for (var i = 0; i < 400; i+=20) {
        stroke(133, 133, 133);
        line(i,0,i,400);
        stroke(0, 0, 0);
    }
    // Data
    fill(255, 255, 255);
    rect(0,0,400,100);
    textSize(20);
    fill(255, 115, 0);
    text("Masse 1: "+ m1,20,25);
    text("Hastighed 1: "+ round(v1),20,60);
    text("Impuls 1: "+ round(p1),20,95);
    fill(31, 91, 255);
    text("Masse 2: " +m2,220,25);
    text("Hastighed 2: " +round(v2),220,60);
    text("Impuls 2: " +round(p2),220,95);
    
    // Bold 1
    fill(255, 115, 0);
    ellipse(x1,200,d1,d1);  
    
    // Bold 2
    fill(31, 91, 255);
    ellipse(x2,200,d2,d2); 
    
    p1=v1*m1;
    x1= x1+v1*dt;
       
    p2=v2*m2;
    x2= x2+v2*dt;
    
        if(x2-d2/2 < x1+d1/2){
            ov1=v1;
            v1 = ov1*((m1-m2)/(m1+m2))+v2*((2*m2)/(m1+m2));
            v2 = ov1*((2*m1)/(m1+m2))+v2*((m2-m1)/(m1+m2));
        }
};
```
Vores bolde opfører sig nu lidt anderledes, prøv at ændre lidt på værdierne og se hvordan de opfører sig.

Hvornår sker disse ting?
* Bold 1 stopper og bold 2 fortsætter.
* Begge bolde fortsætter i samme retning.
* Bold 1 skyder tilbage efter sammenstød

Hvor hurtigt kan bold 2 hurtigts skydes afsted i forhold til bold 1?
