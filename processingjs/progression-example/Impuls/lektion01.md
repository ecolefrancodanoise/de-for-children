Impuls, masse og hastighed
======================================
Impuls er en måde at målle hvor meget "energi" der er i noget der beveger sig

Her skal vi ende med at få 2 bolde til at støde ind i hinanden og se hvad massen (vægten) og hastigheden påvirker vores bolde.

Lad os starte med at lave en bold:

```
// Stor bold
var d1 = 20;
var m1 = 20;
var v1 = 10;
var x1 = 10;

var p1= v1*m1;

var dt = 0.1;

draw = function() {
    background(255, 255, 255);
    
    // Store bold
    fill(255, 115, 0);
    ellipse(x1,200,d1,d1);
    
        p1=abs(v1*m1);
        x1= x1+v1*dt;
};

```
Det er lidt kedeligt med en bold, men hvad skere der nå du ændrer på værdien i 'var v1 = 10;'?

Hvad sker der når du ændrer på andre ting under '// Stor bold'?
* Kan du gætte hvad 'v1' står for?
* Hvad tror du 'm2' står for?

Lad os lave en anden bold:
```
// Stor bold
var d1 = 20;
var m1 = 20;
var v1 = 10;
var x1 = 10;

// Lille bold
var d2 = 5;
var m2 = 5;
var v2 = 0;
var x2 = 60;

var p1= v1*m1;
var p2= v2*m2;

var dt = 0.1;

draw = function() {
    background(255, 255, 255);
    
    // Store bold
    fill(255, 115, 0);
    ellipse(x1,200,d1,d1); 
    
    // Lille bold
    fill(31, 91, 255);
    ellipse(x2,200,d2,d2); 
    
        p1=abs(v1*m1);
        x1= x1+v1*dt;
       
        p2=abs(v2*m2);
        x2= x2+v2*dt;
};
```
Der sker ikke rigtig noget, du kan jo altid ændre på værdien i 'var v2 = 0;', men det er ikke så spændende.

Vi skal nu sørge for at vores bolde støder sammen i stedet for at gå igennem hinanden:

```
// Stor bold
var d1 = 20;
var m1 = 20;
var v1 = 10;
var x1 = 10;

// Lille bold
var d2 = 5;
var m2 = 5;
var v2 = 0;
var x2 = 60;

var p1= v1*m1;
var p2= v2*m2;

var dt = 0.1;

draw = function() {
    background(255, 255, 255);
    
    // Store bold
    fill(255, 115, 0);
    ellipse(x1,200,d1,d1); 
    
    // Lille bold
    fill(31, 91, 255);
    ellipse(x2,200,d2,d2); 
    
        p1=abs(v1*m1);
        x1= x1+v1*dt;
       
        p2=abs(v2*m2);
        x2= x2+v2*dt;
    
        if(x2-d2/2 < x1+d1/2){
            p2=(p2+p1);
            p1=0;
            v2 = (p2/m2);
            v1 = (p1/m1);
        }
};
```
YES! Nu rammer de hinanden!

Hvad sker der hvis vi ændrer deres masse eller hastighed?

Prøv at lege lidt med værdierne for de forskellige bolde.


